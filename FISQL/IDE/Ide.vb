﻿Imports FastColoredTextBoxNS

Public Class Ide
    Dim query As String = ""
    Dim paquete As String = ""
    Dim consolaTxt As FastColoredTextBox
    Dim numTab As Integer = 0

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addTab()
    End Sub

    Private Sub analizarButton_Click(sender As Object, e As EventArgs) Handles analizarButton.Click
        Dim queryTxt As FastColoredTextBox = CType(editorTabControl.SelectedTab.Controls("queryTxt"), FastColoredTextBox)
        query = paqueteSql(queryTxt.Text)
        AsynchronousClient.Main(query)
    End Sub

    Function paqueteSql(ByVal instruccion As String) As String
        paquete = "[ 
                 ""paquete"": ""usql"",
                 ""instrucción"": """ + instruccion + """,
                    ]"
        Return paquete

    End Function

    Sub addTab()
        Dim tabPage As System.Windows.Forms.TabPage = New TabPage()
        Dim queryTxt As FastColoredTextBox = New FastColoredTextBox()
        tabPage.Size = New Point(895, 295)
        If numTab > 0 Then
            tabPage.Text = "Script" + Str(numTab)
        Else
            tabPage.Text = "SQL"
        End If
        queryTxt.Size = New Point(890, 290)
        queryTxt.Name = "queryTxt"
        tabPage.Controls.Add(queryTxt)
        editorTabControl.Controls.Add(tabPage)
        numTab = numTab + 1
    End Sub
End Class
