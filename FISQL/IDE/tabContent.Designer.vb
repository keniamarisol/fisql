﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tabContent
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.editorText = New FastColoredTextBoxNS.FastColoredTextBox()
        Me.SuspendLayout()
        '
        'editorText
        '
        Me.editorText.AutoScrollMinSize = New System.Drawing.Size(25, 15)
        Me.editorText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.editorText.Location = New System.Drawing.Point(12, 12)
        Me.editorText.Name = "editorText"
        Me.editorText.Size = New System.Drawing.Size(928, 469)
        Me.editorText.TabIndex = 2
        '
        'tabContent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(960, 502)
        Me.Controls.Add(Me.editorText)
        Me.Name = "tabContent"
        Me.Text = "tabContent"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents editorText As FastColoredTextBoxNS.FastColoredTextBox
End Class
