﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Ide
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.editorTabControl = New System.Windows.Forms.TabControl()
        Me.consolaTabControl = New System.Windows.Forms.TabControl()
        Me.salidaTabPage = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.mensajesTabPage = New System.Windows.Forms.TabPage()
        Me.historialTabPage = New System.Windows.Forms.TabPage()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.menuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.archivoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.abrirMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.guardarMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.guardarComoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HerramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.analizarButton = New System.Windows.Forms.Button()
        Me.consolaTabControl.SuspendLayout()
        Me.menuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'editorTabControl
        '
        Me.editorTabControl.Location = New System.Drawing.Point(358, 62)
        Me.editorTabControl.Multiline = True
        Me.editorTabControl.Name = "editorTabControl"
        Me.editorTabControl.SelectedIndex = 0
        Me.editorTabControl.Size = New System.Drawing.Size(900, 300)
        Me.editorTabControl.TabIndex = 8
        '
        'consolaTabControl
        '
        Me.consolaTabControl.Controls.Add(Me.salidaTabPage)
        Me.consolaTabControl.Controls.Add(Me.TabPage3)
        Me.consolaTabControl.Controls.Add(Me.mensajesTabPage)
        Me.consolaTabControl.Controls.Add(Me.historialTabPage)
        Me.consolaTabControl.Location = New System.Drawing.Point(357, 371)
        Me.consolaTabControl.Multiline = True
        Me.consolaTabControl.Name = "consolaTabControl"
        Me.consolaTabControl.SelectedIndex = 0
        Me.consolaTabControl.Size = New System.Drawing.Size(900, 301)
        Me.consolaTabControl.TabIndex = 9
        '
        'salidaTabPage
        '
        Me.salidaTabPage.AutoScroll = True
        Me.salidaTabPage.Location = New System.Drawing.Point(4, 22)
        Me.salidaTabPage.Name = "salidaTabPage"
        Me.salidaTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.salidaTabPage.Size = New System.Drawing.Size(892, 275)
        Me.salidaTabPage.TabIndex = 0
        Me.salidaTabPage.Text = "Salida de Datos"
        Me.salidaTabPage.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.AutoScroll = True
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(892, 275)
        Me.TabPage3.TabIndex = 1
        Me.TabPage3.Text = "Plan de Ejecución"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'mensajesTabPage
        '
        Me.mensajesTabPage.AutoScroll = True
        Me.mensajesTabPage.Location = New System.Drawing.Point(4, 22)
        Me.mensajesTabPage.Name = "mensajesTabPage"
        Me.mensajesTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.mensajesTabPage.Size = New System.Drawing.Size(892, 275)
        Me.mensajesTabPage.TabIndex = 2
        Me.mensajesTabPage.Text = "Mensajes"
        Me.mensajesTabPage.UseVisualStyleBackColor = True
        '
        'historialTabPage
        '
        Me.historialTabPage.AutoScroll = True
        Me.historialTabPage.Location = New System.Drawing.Point(4, 22)
        Me.historialTabPage.Name = "historialTabPage"
        Me.historialTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.historialTabPage.Size = New System.Drawing.Size(892, 275)
        Me.historialTabPage.TabIndex = 3
        Me.historialTabPage.Text = "Historial"
        Me.historialTabPage.UseVisualStyleBackColor = True
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(13, 67)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(298, 288)
        Me.TreeView1.TabIndex = 10
        '
        'TreeView2
        '
        Me.TreeView2.Location = New System.Drawing.Point(13, 372)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(298, 279)
        Me.TreeView2.TabIndex = 11
        '
        'menuStrip1
        '
        Me.menuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.menuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.archivoMenuItem, Me.HerramientasToolStripMenuItem})
        Me.menuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip1.Name = "menuStrip1"
        Me.menuStrip1.Size = New System.Drawing.Size(1370, 24)
        Me.menuStrip1.TabIndex = 12
        Me.menuStrip1.Text = "menuStrip"
        '
        'archivoMenuItem
        '
        Me.archivoMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.abrirMenuItem, Me.guardarMenuItem, Me.guardarComoMenuItem})
        Me.archivoMenuItem.Name = "archivoMenuItem"
        Me.archivoMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.archivoMenuItem.Text = "Archivo"
        '
        'abrirMenuItem
        '
        Me.abrirMenuItem.Name = "abrirMenuItem"
        Me.abrirMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.abrirMenuItem.Text = "Abrir"
        '
        'guardarMenuItem
        '
        Me.guardarMenuItem.Name = "guardarMenuItem"
        Me.guardarMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.guardarMenuItem.Text = "Guardar"
        '
        'guardarComoMenuItem
        '
        Me.guardarComoMenuItem.Name = "guardarComoMenuItem"
        Me.guardarComoMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.guardarComoMenuItem.Text = "Guardar Como"
        '
        'HerramientasToolStripMenuItem
        '
        Me.HerramientasToolStripMenuItem.Name = "HerramientasToolStripMenuItem"
        Me.HerramientasToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
        Me.HerramientasToolStripMenuItem.Text = "Herramientas"
        '
        'analizarButton
        '
        Me.analizarButton.Location = New System.Drawing.Point(13, 33)
        Me.analizarButton.Name = "analizarButton"
        Me.analizarButton.Size = New System.Drawing.Size(80, 28)
        Me.analizarButton.TabIndex = 13
        Me.analizarButton.Text = "Analizar"
        Me.analizarButton.UseVisualStyleBackColor = True
        '
        'Ide
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 711)
        Me.Controls.Add(Me.analizarButton)
        Me.Controls.Add(Me.menuStrip1)
        Me.Controls.Add(Me.TreeView2)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.consolaTabControl)
        Me.Controls.Add(Me.editorTabControl)
        Me.Name = "Ide"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.consolaTabControl.ResumeLayout(False)
        Me.menuStrip1.ResumeLayout(False)
        Me.menuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents editorTabControl As TabControl
    Private WithEvents consolaTabControl As TabControl
    Private WithEvents salidaTabPage As TabPage
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents TreeView2 As TreeView
    Private WithEvents menuStrip1 As MenuStrip
    Private WithEvents archivoMenuItem As ToolStripMenuItem
    Private WithEvents abrirMenuItem As ToolStripMenuItem
    Private WithEvents guardarMenuItem As ToolStripMenuItem
    Private WithEvents guardarComoMenuItem As ToolStripMenuItem
    Friend WithEvents analizarButton As Button
    Private WithEvents TabPage3 As TabPage
    Private WithEvents mensajesTabPage As TabPage
    Private WithEvents historialTabPage As TabPage
    Friend WithEvents HerramientasToolStripMenuItem As ToolStripMenuItem
End Class
